﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A button. Clase abstracta que oculta el registro en el InputMgr de los botones para que estos sean llamados cuando sol pulsados.
/// </summary>
public abstract class Button : AComponent {
	
	protected override void Start()
	{
		base.Start();
		//Registro
		//TODO : Registro 
	}
	
	
	protected override void OnDestroy()
	{
		base.OnDestroy();
		//Desregistro
		//TODO : Desregistrar
		
		//Compruebo que el inputMgr no haya sido ya destruido... (no podemos NI DEBEMOS saber el orden de destruccion de las entidades que hace Unity)
		//if(InputMgr... != null)
			
	}
	
	//Metodos a sobrescribir por los Botones concretos.
    public virtual void OnButtonBeginPressed(){}
    public virtual void OnButtonPressed(){}
    public virtual void OnButtonEndPresseed(){}
    public virtual void OnButtonBeginOver() { }
    public virtual void OnButtonEndOver() { }
}

