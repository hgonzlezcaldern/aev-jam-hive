using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// Ventana que muestra las escenas del build setting para facilitar la carga
/// </summary>
public class EditorLevelLoader : EditorWindow
{
    private static List<bool> m_ActiveButtons;
    private static int m_Length = 0;

    // Menu item (EditorLevelLoader)

    static void Init()
    {
        // Obtiene la ventana existente y, si no, hace una nueva
        // Es necesario para mostrar la ventana
        EditorWindow.GetWindow(typeof(EditorLevelLoader));
        Setup();
    }

    /// <summary>
    /// Configuración de las variables internas
    /// </summary>
    static void Setup()
    {
        m_Length = EditorBuildSettings.scenes.Length;
        m_ActiveButtons = new List<bool>(m_Length);

        for (int i = 0; i < m_Length; ++i)
        {
            m_ActiveButtons.Add(false);
        }
    }

    /// <summary>
    /// Dibujado dentro de la ventana
    /// </summary>
    void OnGUI()
    {
        Setup();

        if (m_Length > 0)
        {

            //TODO creamos los botones, uno por escena almacenada

            GUILayout.Space(30.0f);

            string initialScenePath = EditorBuildSettings.scenes[0].path;
            if (GUILayout.Button("\nPlay " + Path.GetFileNameWithoutExtension(initialScenePath) + "\n"))
            {
                LoadSceneInEditor(initialScenePath, true);
            }

            for (int i = 0; i < m_Length; ++i)
            {
                if (m_ActiveButtons[i])
                {
                    Debug.Log("Loading scene in Editor: " + Path.GetFileNameWithoutExtension(EditorBuildSettings.scenes[i].path));
                    LoadSceneInEditor(EditorBuildSettings.scenes[i].path);
                    Debug.Log("Scene loaded");
                }
            }
        }
        else
        {
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField("No scenes on the BuildSettings!", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            GUILayout.FlexibleSpace();
        }
    }

    /// <summary>
    /// Carga una escena en el editor
    /// </summary>
    /// <param name="scenePath">Ruta de la escena</param>
    /// <param name="playScene">Si se quiere que se de al "play" de la escena, falso si no se quiere</param>
    void LoadSceneInEditor(string scenePath, bool playScene = false)
    {
        //TODO 2 lanzamos la escena no sin antes guardar la actual.
    }


    /// <summary>
    /// Pone el foco en la ventana de jerarquía
    /// </summary>
    void FocusHierarchy()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(EditorWindow), true, "Hierarchy", true);

        if (window)
        {
            window.Show(true);
        }
    }
}